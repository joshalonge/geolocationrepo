import exiftool, subprocess
# This script extracts metadata, more specifically, geolocation from a video file. 
# **** Exiftool must be run for this service to work. *****

# Download Exiftool at https://exiftool.org

# To start exiftool, either run this line of code : subprocess.run('exiftool')
# or open cmd/terminal and type : exiftool 

file = None
running = True 

# Run this program until user inputs "stop"
while running:    
    try: 
        # Declare exiftool as "et" to make typing the name more convenient
        with exiftool.ExifTool() as et: 
            # take file as input
            file = input("Enter the file file in the format filefile.extension. Type q if you wish to stop : ")
            # print geolocation in this format : (1, Latitude: X, Longitude: Y) {success})
            print("(1, " + str(et.get_tag("GPSLatitude", file)) + ", " + str(et.get_tag("GPSLongitude", file)) + ") {success} ")      
    # If the input is the wrong format, throw an exception of "Invalid input" 
    except ValueError: 
        print("Invalid Input {failure}")
    # If user enters "Q", stop the program
    if file == ("Q").lower():
        print("Stopping program!")
        running = False
